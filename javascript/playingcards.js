
var suits = ["Diamonds", "Clubs", "Hearts", "Spades"];
var values = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"];

function Card(value, suit){
  if(suit===undefined){suit = suits[Math.floor(Math.random()*4)];}
  if(value===undefined){value = values[Math.floor(Math.random()*13)];}

  this.suit = suit;
  this.value = value;
}

Card.prototype.toString = function(){
  return this.value + " of " + this.suit;
}

function pickARandomCard(){
  var randomCard = new Card();
  document.getElementById("cardObtained").innerHTML = randomCard.toString();
}
